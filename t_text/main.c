#include "blc_core.h"

#define SIZE_TO_WRITE 1000456670 //Random value to display in a more human friendly format

int main(){
    int rows_nb, columns_nb;
    float values[3]={1.3, 4.2, 5.5};
    
    //We use stderr as output each time it concerns information for the user but does not have to be interpreted or parsed by a program.
    fprintf(stderr, "\n");
    underline_fprintf('=', stderr, "Testing blc_text");
    
    color_fprintf(BLC_BLUE, stderr, "\nWriting in blue\n\n");
    underline_fprintf('-', stderr,  "Writing underlining with '-'");
    fprintf(stderr, "Writing  3 floats of an array as tsv format. It can be imported in excel or equivalent.\n");
    fprint_tsv_floats(stderr, values, 3);
    //Getting the size in characters of the current terminal
    blc_terminal_get_size(&columns_nb, &rows_nb);
    fprintf(stderr, "\nSize of the terminal  %dx%d\n", columns_nb, rows_nb);
    //Writing size in human readable format
    fprintf(stderr, "%d can be written as:", SIZE_TO_WRITE);
    fprint_human_size(stderr, SIZE_TO_WRITE); //1Kb = 1024b
    printf("\n");
    return EXIT_SUCCESS;
}
