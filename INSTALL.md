INSTALL
=======

You are advised to used standard [blaar install](https://framagit.org/blaar/blaar/wikis/INSTALL) but you are free to make a manual installation

Manual install with cmake
-------------------------
You need git, g++, cmake and doxygen for the documentation:
- Ubuntu: 

```sh
sudo apt-get install git g++ cmake
```
- OSX with [homebrew](http://brew.sh) and any C++ compiler

```sh
brew install git cmake
```
You can copy past the following code:

```sh
git clone https://framagit.org/blaar/blc_core.git
cd blc_core
mkdir build
cd build
cmake ..
make
sudo make install
```
The created library `libblc_core.{so|dylib}` and `libblc_core.a` will be in `/usr/local/lib`.  Includes in `/usr/local/include/blc_core` and a cmake config file in `/usr/local/share/blc_core`
 
You can create a debian package (.deb) with:

    make package

Manual install with simple makefiles
====================================

All the sources are in src, includes in include and the previous lines allow you to create an adequate makefile
