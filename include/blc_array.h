/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: A. Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#ifndef BLC_ARRAY_H
#define BLC_ARRAY_H

#include "blc_tools.h"
#include "blc_mem.h"

/**
 @defgroup blc_array blc_array
 @{
 @brief blc_mem with more informations about type, format and structures (dimensions) of the data.
 */

/**Like EXIT_ON_ERROR but displays all the informations about the array on stderr before quitting. Useful to debug.*/
#define EXIT_ON_ARRAY_ERROR(array, ...) blc_array_fatal_error(array, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

///Description of a dimension properties of a blc_array.
typedef struct blc_dim {
    size_t length; ///<number of element for the dimension
    size_t step; ///< shift in bytes to do to pass from one element of this dimension to the next one
    

} blc_dim;

START_EXTERN_C
blc_dim *vcreate_blc_dims(size_t *size, uint32_t type, int dims_nb, int length, va_list arguments);

blc_dim *create_blc_dims(size_t *size, uint32_t type, int dims_nb, int length0, ...);
END_EXTERN_C

///Description of a n-dimensional array with type and format
typedef struct blc_array
#ifdef __cplusplus
:blc_mem {
    
    /**Define an empty array*/
    blc_array();
    /**Define and allocates the array */
    blc_array(uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /**Free dims. The data is freed by ~blc_mem()*/
    ~blc_array();
    
/* Definiting the array without allocation. Useful when you want to use external buffer
 ====================================================================================*/
    
    /**Defines the blc_array but does not allocate memory (data)*/
    void vdef_array(uint32_t type, uint32_t format, int dims_nb, int length0, va_list arguments);
    /**Defines the blc_array but does not allocate memory (data)*/
    void def_array(uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /**Defines the blc_array but does not allocate memory (data)*/
    void def_array(uint32_t type, uint32_t format, int dims_nb, blc_dim *dims);
    /**Defines the blc_array but does not allocate memory (data)*/
    void def_array(uint32_t type, uint32_t format, char const *dims_string);
    
    
/* Allocating the array
 ====================*/
    
    /**Defines and allocates the blc_array with a string (i.e. "UIN8 RGB3 3x800x600")*/
    void init(char const *properties);
    /**Defines and allocates the blc_array */
    void vinit(uint32_t type, uint32_t format, int dims_nb, int length, va_list arguments);
    /**Defines and allocates the blc_array */
    void init(uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    
/* Printing informations about the blc_array
==========================================*/
    
    /**Print the properties of the dims in a string (i.e. "3x800x600" )*/
    int sprint_dims(char *string, int max_string_size);
    /**Print the properties of the dims in a file or stderr by default (i.e. "3x800x600" )*/
    int fprint_dims(FILE *file=stderr) const;
    /**Print all the informations about the blc_array. Useful for debuging.*/
    void fprint_debug(FILE *file=stderr) const;
    /**Print all the properties of the array in a string (i.e. "UIN8 RGB3 3x800x600")*/
    void sprint_properties(char *string, size_t max_string_size);
    /**Print all the properties of the array in a file or stderr by default. (i.e. "UIN8 RGB3 3x800x600")*/
    void fprint_properties(FILE *file=stderr);
    
/* Modifying the properties of the array
======================================*/

    /**Add one dimention to the definition of the blc_array. You have to  manage  eventual **memory reallocation** yourself.*/
    void add_dim(int length, int step);
    /**Add one dimension ti the definition of the blc_array. The step is infered from the type of the data and the size of the previous dimension. You have to manage the eventual **memory reallocation** yourself.*/
    void add_dim(int length);
    /** Set all the dims of the array*/
    void set_dims(int dims_nb, int length, ...);
    /**Set all dims by reading the properties from the string  (i.e. "3x800x600" ).  You have to  manage  eventual **memory reallocation** yourself. */
    int sscan_dims(char const* string);
    /**Set all dims by reading the properties from the file  (i.e. "3x800x600" ).  You have to  manage  eventual **memory reallocation** yourself. */
    void fscan_dims(FILE *file);
    /**Set all properties by reading the properties from the string  (i.e. "UIN8 RGB3 3x800x600" ).  You have to  manage  eventual **memory reallocation** yourself. */
    void sscan_properties(char const *string);
    /**Set all properties by reading the properties from the file  (i.e. "UIN8 RGB3 3x800x600" ).  You have to  manage  eventual **memory reallocation** yourself. */
    void fscan_properties(FILE *file);
   // size_t get_minimum_size();

/*Reading data from files
 ======================*/
    
    /**Defines the array without allocating the data by reading the file with .blc extension*/
    void def_with_blc_file(char const *filename);
    /**Defines and allocates the blc_array depending on the description of the blc file (with .blc extension) but does not update the data. This allow you to refresh data without reallocating memory.*/
    void init_with_blc_file(char const *filename);
    /**Update the content of the memory with the content of the blc file. The blc_array has to be properly defined and allocated before. .blc format is a specific format, whre de first line is the properties of the blc_array and them it is raw binary memory. It may be a problem with endianness */
    void update_with_blc_file(char const *filename);

    void def_with_tsv_file(char const *filename);
    /**Same as update_with_blc_file but this reas .tsv, tab separated values (i.e. 0.75     0.33    0.55    ). Typically used with excel and similar. You have to care yourself to define and allocate an appropriate blc_array.*/
    void update_with_tsv_file(char const *filename);

/*Writing data in files
=====================*/
    
    /**Create a .blc file with the content of the blc_array. This is meant to be used with init_with_blc_file, update_with_blc_file.*/
    void save_blc_file(char const *filename);
    /**Write the content of the blc_array as TSV (tab separated values) in a file. It can also be used to write on the terminal.*/    
    void fprint_tsv(FILE *file=stderr);
    /**Comme fprint_tsv but automtically create the file. With filename which must have .tsv extension.*/
    void save_tsv_file(char const *filename);
    /**Print the blc_array as a text_graph surface. Only works with 2D blc_array of type 'UIN8'. It draws a matrix of values. If ansi terminal is 1, it draws colored valuse depending on the blc_uchar_color_scale.*/
    void fprint_surface_uchars(FILE *file, int ansi_terminal=0);

/*miscellaneous
=============*/
    
    /**Return the size in bytes of one element depending on the type of data of the blc_array*/
    int get_type_size();
    
#else
    {
        blc_mem mem; ///< raw memory of the array
#endif
        uint32_t type; ///< type of data in the memory as a unsigned int of 4 bytes. Possibilities are 'UIN8' uchar, 'INT8' char, 'UI16' uint16_t, 'IN16' int16_t, 'UI32' uint32_t, 'IN32' int32_t, 'FL32' float, 'FL64' double
        uint32_t format; ///< describes how the data should be interpreted. 'NDEF' (no specification), 'TEXT' for strings, 'Y800' black and white image, 'RGB3' image in RGB, 'LPCM' sound in non compressed format, ...
        blc_dim * dims; ///< array of dimentions of the blc_array
        int dims_nb; ///< number of dimension of the blc_array
        size_t total_length; ///<Total number of elements in the array. This differ of size if an element is bigger than one byte.
    } blc_array;
    
/*Plain C functions ( work also with C++)
===============================*********/
    
    START_EXTERN_C
    /**Define but does not allocate the blc_array.*/
    void blc_array_def(blc_array *blc_array,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /**Define and allocate the blc_array*/
    void blc_array_init(blc_array *blc_array,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /** like blc_fatal_error but add informations about the blc_array.  You should not call it, it is called by EXIT_ON_ARRAY_ERROR*/
    void blc_array_fatal_error(blc_array const *array, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
    /**Free the dims description and data if not null of the array*/
    void blc_array_destroy(blc_array *array);
    END_EXTERN_C

    ///@}
#endif
    
