/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: Arnaud Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.*/

/**
@mainpage Summary

Blc core is composed of these five modules (one file each):  
-  @ref blc_text manipulates strings, text files and terminal interaction (colors, size, ...).
-  @ref blc_tools various low level tools and shortcuts, error checks, simple memory management, ...
-  @ref blc_realtime few functions to mesure time, manage pthreads and semaphores.
-  @ref blc_mem manipulates simple blocks of memory with reallocations.
-  @ref blc_array manipulates memory as n-dimentional arrays allowing to represent sound, images or anykind of data.
 
*/

#ifndef BLC_TEXT_H
#define BLC_TEXT_H

#include <stdio.h>
#include <stdarg.h>
#include "blc_tools.h"
#include "blc_mem.h"

/**
 @defgroup blc_text blc_text
 @{
 
 @brief Provide tools to manipulate text on files or on the terminal (color, formating, ...)
 Consider using ncurses ( https://www.gnu.org/software/ncurses ) for more complex interactive tools with the terminal.
 */

#define MOVE_BEGIN_PREVIOUS_N_LINES "%dF"

#define DEL_END_SCREEN "0J"
#define DEL_BEGIN_SCREEN "1J"

#define DEL_BEGIN_LINE "1K"
#define DEL_ALL_LINE "2K"

///Number of gradual colors we can use.
#define BLC_BAR_COLORS_NB 12

#define BLC_BRIGHT 0x8

///Code to be used in text coloring function (color_id).
enum BLC_COLORS{
  BLC_BLACK = 0,
  BLC_RED = 1,
  BLC_GREEN = 2,
  BLC_YELLOW = 3,
  BLC_BLUE = 4,
  BLC_MAGENTA = 5,
  BLC_CYAN = 6,
  BLC_WHITE = 7,
  BLC_GREY = 8,
  BLC_BRIGHT_RED = 9,
  BLC_BRIGHT_GREEN = 10,
  BLC_BRIGHT_YELLOW = 11,
  BLC_BRIGHT_BLUE = 12,
  BLC_BRIGHT_MAGENTA = 13,
  BLC_BRIGHT_CYAN = 14,
  BLC_BRIGHT_WHITE = 15,
  BLC_COLORS_NB
};

///eprint for info print (stderr) to oppose standard cursor (stdout) usually not used
#define blc_eprint_cursor_position(row, column) eprintf_escape_command("%d;%dH", row, column);
#define blc_eprint_cursor_up(rows_nb) eprintf_escape_command("%dA", rows_nb);
#define blc_eprint_cursor_down(rows_nb) eprintf_escape_command("%dB", rows_nb);
#define blc_eprint_cursor_up_left(rows_nb) eprintf_escape_command("%dF", rows_nb);
#define blc_eprint_cursor_down_right(rows_nb) eprintf_escape_command("%dE", rows_nb);
#define blc_eprint_del_end_line() eprintf_escape_command("0K");
#define blc_eprint_del_end_screen() eprintf_escape_command("0J");
#define blc_fprint_del_end_line(file) fprintf_escape_command(file, "0K");

///Shortcut to escape directly on stdout
#define  printf_escape_command( ...) if (blc_stdout_ansi) fprintf_escape_command(stdout,  __VA_ARGS__)
///Shortcut to escape directly on stderr
#define  eprintf_escape_command( ...) if (blc_stderr_ansi) fprintf_escape_command(stderr,  __VA_ARGS__)
///Start a new color for the standart output.
#define  print_start_color(color_id) fprint_start_color(stdout, color_id)
///Reset the default color for the standart output.
#define print_stop_color() fprint_stop_color(stdout)
///Shortcut to print directly on stdout with color only if stdout accept color
#define color_printf(color_id, ...) do{if(blc_stdout_ansi) color_fprintf(color_id, stdout, __VA_ARGS__); else fprintf(stderr, __VA_ARGS__);}while(0)
///Shortcut to print directly on stderr only if stderr accept colors
#define color_eprintf(color_id, ...) do{if(blc_stderr_ansi) color_fprintf(color_id, stderr, __VA_ARGS__); else fprintf(stderr, __VA_ARGS__);}while(0)
///Same as color_eprintf but take a va_list of arguments like vprintf
#define color_veprintf(color_id, ...) do{if(blc_stderr_ansi) color_vfprintf(color_id, stderr, __VA_ARGS__); else vfprintf(stderr, __VA_ARGS__);}while(0)
///Apply underline on stdout
#define underline_printf(char_pattern, ...) underline_fprintf( char_pattern, stdout, __VA_ARGS__)
///Apply human size on stderr
#define eprint_human_size(size) fprint_human_size(stderr, size)
///apply on stdout
#define print_tsv_floats(values, values_nb) fprint_tsv_floats(stdout, values, values_nb);
///apply on stdout
#define scan_tsv_floats(values, values_nb) fscan_tsv_floats(stdin, values, values_nb);
///Variable setting wether we can use ANSI escape codes. This value is true by default, use terminal_ansi_detect() to set its value.
extern int blc_stdout_ansi;
///Variable setting wether we can use ANSI escape codes. This value is true by default, use terminal_ansi_detect() to set its value.
extern int blc_stderr_ansi;
///Contain the color_id going gradually from dark_blue to dark_red corresponding to @ref BLC_BAR_COLORS_NB scales.
extern uchar blc_bar_colors[BLC_BAR_COLORS_NB];

START_EXTERN_C
///Try to get the position of the cursor on the terminal.
int blc_fterminal_try_to_get_cursor_position(FILE *file, int *x, int *y);
///Test wether the terminal is ANSI. color etc ..
int blc_stdout_ansi_detect();
///Detect if stderr accept ansi escape command (i.e. color). If not a message is displyed in the terminal
int blc_stderr_ansi_detect();
///Get the size of the terminal windows in characters.
void blc_terminal_get_size(int *columns, int *lines);
///Set a callback to be called each time the terminal window is resized.
void blc_terminal_set_resize_callback(void (*callback)(int columns_nb, int rows_nb, void* user_data), void *user_data);
///Send an escape command on the terminal.
void fprintf_escape_command(FILE *file, char const *format, ...);
///Set a new color for the terminal.
void fprint_start_color(FILE *file, int color_id);
///Reset the default color for the terminal
void fprint_stop_color(FILE *file);
///Change the backgroungd color.
void fprint_backgournd_start_color(FILE *file, int color_id);
///Set back the background color to default.
void fprint_backgournd_stop_color(FILE *file);
///Print a text in color.
void color_fprintf(int color_id, FILE *file, char const *format, ...);
///Put a color text in a char string
int color_sprintf(int color_id, char *string, const char *format, ...);
///Print a text in color.
void color_vfprintf(int color_id, FILE *file, char const *format, va_list arguments);
///Put a color text in a char string
int color_vsprintf(int color_id, char *string, char const *format, va_list arguments);
///Underline with the pattern char the text given in parameter.
void underline_fprintf(char pattern, FILE * file, char const *format, ...);
///Print to the file a human readable format of the size ( Kb, Mb, Gb, ...).
void fprint_human_size(FILE *file, size_t size);
///Print in tab separated values the array of values.
void fprint_tsv_floats(FILE *file, float *values, int values_nb);
///Read tab separated floats in the files and set values with the result.
void fscan_tsv_floats(FILE *file, float *values, int values_nb);
///Read tab separated uchars in the files and set values with the result.
void fscan_tsv_uchars(FILE *file, uchar *values, int values_nb);

void blc_fprint_char_graph(FILE *file, char *values, int values_nb, char const *title, int width, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name);
void blc_fprint_float_graph(FILE *file, float *values, int values_nb, char const *title, int width, int height, float max, float min,  char const* abscissa_name,  char const* ordinate_name);

/**Display a surface graph of a 3D array in text mode ( tipically a image). If step0 and lenght0 = 1 it works with a 2D array.
The parameters are a bit complex, you are advised to use higher level functions like: blc_array::fprint_surface_uchars in C++
 @param file where to put the graph usually stderr
 @param data pointer contating data (uchar) to draw
 @param size size of the data to be displayed
 @param offset shift the original pointerof offset bytes. Usefule to select the channel of a color image
 @param step0 step of the lower dimention. Typically 3 to display one channel of a RGB3 image, 2 to display the luminosity of a YUYV image, 1 for a black and white image.
 @param length0 number of elements of the low dimension. Usualy 1 for pixels.
 @param step1 step of the second dimension. Typically the pixel size of an image but it can be more to scale the image.
 @param length1 number of elements of the second dimention. For non scalled image, it is width.
 @param step2 step of the third dimension. Typically the pixel size multiplied by the width of an image. More to scale the image.
 @param length2 number of elements of the third dimention. For non scalled image, it is height.
 @param ansi_terminal flag to say if it can use ansi escape code (typically colors).*/
void blc_fprint_3Darray(FILE *file, uchar *data, size_t size, int offset, int step0, int length0, int step1, int length1,  int step2,  int length2, int ansi_terminal);
///Print a color scale usefull for fprint_graph
void blc_fprint_color_scale(FILE *file);
///Set stdin (keyboard) in non blocking mode
void blc_set_stdin_non_blocking_mode();
///Set back stdin as it was before calling blc_set_stdin_non_blocking_mode
void blc_set_back_stdin_mode();
END_EXTERN_C
///@}
#endif
