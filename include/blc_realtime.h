/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#ifndef BLC_REALTIME_H
#define BLC_REALTIME_H

#include "blc_tools.h"
#include <semaphore.h>

/**
 @defgroup blc_realtime blc_realtime
 Few functions and shortcut helping for pseudo (standard unix is not realtime anyway) realtime applications
@{*/
///Maximum size of a semaphore name
#define BLC_SEM_NAME_LEN 31 // At least on OSX

/**Write the appropriate message error and exit the program for functions error returned by pthread_... functions. It is similar to EXIT_ON_SYSTEM_ERROR but for pthread_... functions*/
#define BLC_EXIT_ON_PTHREAD_ERROR(blc_pthread_errno, ...) blc_fatal_pthread_error(__FILE__, __FUNCTION__, __LINE__, blc_pthread_errno, NULL, __VA_ARGS__)

/**Execute the command which should be something like 'pthread_...', check the return value and display the error message and exit on error. Error_variable is any int variable you need to provide to the macro. It will be set to 0 in case of success.
 It is a good habit to always check the return value of this function. Exemple:
@code{.c}
 int error_id;
 pthread_t thread;
 PTHREAD_CHECK(pthread_create(&thread, NULL, your_callback, your_callback_argument), error_id, "Failling creating thread ...");
@endcode
In case of error, you will know the line, the file, the command that was executed and the error in human readable form.*/
#define BLC_PTHREAD_CHECK(command, ...) do{if (((blc_pthread_errno)=(command))) blc_fatal_pthread_error(__FILE__, __FUNCTION__, __LINE__, blc_pthread_errno, STRINGIFY(command), __VA_ARGS__);}while(0)

START_EXTERN_C

///Like errno for system error id, it store error id for pthread.
extern int blc_pthread_errno;

///This is called by EXIT_ON_PTHREAD_ERROR and PTHREAD_CHECK, you should not need it directly.
void blc_fatal_pthread_error(const char *name_of_file, const char* name_of_function, int numero_of_line, int error_id, const char *command, const char *message, ...);
///Return the time difference between the timeval struct and the actual time and set the time to the current time.
long blc_us_time_diff(struct timeval *time);
///Check weather a semaphore is locked. You may use sem_getvalue instead but it does not exist on darwin (Mac OSX).
int blc_sem_available(sem_t *sem);
/**Try to lock the mutex in timeout micro seconds.
 @return 1 in case of immediate success, -1 in case of succes after microseconds, 0 in case of failure */
int blc_mutex_trylock_in_time(pthread_mutex_t *mutex, uint32_t microseconds);
END_EXTERN_C
///@}
#endif

