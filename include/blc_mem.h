/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: A. Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */



#ifndef BLC_MEM_H
#define BLC_MEM_H

#include "blc_tools.h"
#include <stdio.h>

/**
 @defgroup blc_mem blc_mem
 @{ 
 @brief Provides simple tools to dynamically manipulate memory.
 
 This kind of functionnalities are more complete with the Standard Template Library (https://en.wikipedia.org/wiki/Standard_Template_Library) but it much simpler here and works with plain C.

 blc_mem is included in blc_array
 */

/** Description of a block of memory with a pointer and a size.*/
typedef struct blc_mem {
    ///The union is used to cast the pointer in whatever you want. It does NOT mean you allocate n pointers.
    union{
        void *data;
        char *chars;
        uchar *uchars;
        int16_t *ints16;
        uint16_t *uints16;
        int32_t *ints32;
        uint32_t *uints32;
        float *floats;
        double *doubles;
    };
    size_t size; ///<Size of the memory in bytes
    
#ifdef __cplusplus
    /**Initiallize blc_mem with empty data and size*/
    blc_mem();
    /** Create and allocate memory of size size */
    blc_mem(size_t size);
    /// Free the data. Only if data is not NULL and size !=0; It is a way to not delete data we want to keep.
    ~blc_mem();
    /**really allocates the data of size previously set*/
    void allocate();
    /** Change the size of memory. The content is not preserved. Use size = 0 to free the memory. */
    void allocate(size_t size);
    /// Allocate more memory if needed but does not free memory
    void allocate_min(size_t size);
    /** Change the size of memory. But keep the content.*/
    void reallocate(size_t size);
    /** Increases the memory of size and adds the new content.*/
    void append(char const *new_data, size_t new_data_size);
    /** Like append but automatically detect the size with the null char*/
    void append_text(char const *new_text);
    /** Replace the memory with the new data and size*/
    void replace(char const *new_data, size_t size);
    /** Reset all the value to 'value'. */
    void reset(int value=0);
    
    /** Draw text only the graph usually on a terminal based on the uchars data. If you want to draw more than one graph, use blc_mems_fprint_graph_uchars
     @param file  ouptut where to graph (usually stderr)
     @param title title of the graph
     @param height height of the text graph in lines of text
     @param max top value drawn. It is in this order (max first) to avoid to set min when it is 0
     @param min is the bottom value drawn
     @param abscissa_name text displayed in the abscissa. It as to be the same for all graphs
     @param ordinate_name text displayed in ordonate
     */
    void fprint_graph_uchars(FILE *file, char const *title, int height, int max=255, int min=0,  char const* abscissa_name=NULL, char const* ordinate_name = NULL);
    /** Draw text only the graph usually on a terminal based on the float data.
     @param file  ouptut where to graph (usually stderr)
     @param title title of the graph
     @param height height of the text graph in lines of text
     @param max top value drawn. It is in this order (max first) to avoid to set min when it is 0
     @param min is the bottom value drawn
     @param abscissa_name text displayed in the abscissa. It as to be the same for all graphs
     @param ordinate_name text displayed in ordonate
     */
    void fprint_graph_floats(FILE *file, char const *title, int height, float max=1.0f, float min=0.f,  char const* abscissa_name=NULL, char const* ordinate_name = NULL);

#endif
}blc_mem;

#ifndef __cplusplus
#define BLC_MEM_INITIALIZER {.data=NULL, .size=0}
#endif


START_EXTERN_C
/// Allocate or reallocate memory as requested by **size** but lose the content.
void blc_mem_allocate(blc_mem *mem, size_t size);
/// Reallocate memory and keep the content.
void blc_mem_reallocate(blc_mem *mem, size_t size);
/// Adapt the size and replace the content with the new one
void blc_mem_replace(blc_mem *mem, char const* data, size_t size);
/// Increase the size and append the data at the end.
void blc_mem_append(blc_mem *mem, char const* data, size_t size);
/// Draw text graphs usually on the terminal based and blc_mems uchars.
/**
@param mems pointer to an array of blc_mem to graph
@param mems_nb number of blc_mems to graph
@param file ouptut where to graph (usually stderr)
@param titles array of graph titles
@param height height of the text graph in lines of text
@param max top value drawn. It is in this order (max first) to be consistent with c++ version
@param min is the bottom value drawn
@param abscissa_name text displayed in the abscissa. It as to be the same for all graphs
@param ordinate_name text displayed in ordonate
*/
void blc_mems_fprint_graph_uchars(blc_mem *const *mems, int mems_nb, FILE *file, char const *const* titles, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name );

/// Draw text graphs usually on the terminal based and blc_mems uchars.
/**
 @param mems pointer to an array of blc_mem to graph
 @param mems_nb number of blc_mems to graph
 @param file ouptut where to graph (usually stderr)
 @param titles array of graph titles
 @param height height of the text graph in lines of text
 @param max top value drawn. It is in this order (max first) to be consistent with c++ version
 @param min is the bottom value drawn
 @param abscissa_name text displayed in the abscissa. It as to be the same for all graphs
 @param ordinate_name text displayed in ordonate
 */
void blc_mems_fprint_graph_floats(blc_mem *const *mems, int mems_nb, FILE *file, char const *const* titles, int height, float max, float min,  char const* abscissa_name,  char const* ordinate_name );

END_EXTERN_C
//@}
#endif


