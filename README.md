BLC core : Core of Basic Libraries for C/C++ 
============================================

- Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, CNRS (2011-2016)
- Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)
- Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)

Core functions used by almost all the projects of [BLAAR](https://framagit.org/blaar/blaar)

It is composed of five main files (need a POSIX system), for Linux and OSX.

- **blc_text** manage ASCII terminals (colors, cursor, sizes)
- **blc_tools** macros and functions to simplify coding and memory management
- **blc_mem** a memory structure (data pointer and size) essentially to manage dynamic memory buffers
- **blc_array** a structure to manage n-dimensional arrays. It also manage type and format of the data
- **blc_realtime** few helpers to use time, POSIX semaphore and pthreads

[Standard blaar install](https://framagit.org/blaar/blaar/wikis/install) automatically installs it.

However, independently of blaar, you can [manually build the library](https://framagit.org/blaar/blc_core/blob/master/INSTALL)  


[More details and examples](https://framagit.org/blaar/blc_core/wikis/home)
