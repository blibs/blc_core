# - Try to find blc_core used by find_package()
# Once done this will define
#  BLC_CORE_FOUND - System has blc_core
#  BL_INCLUDE_DIRS - The blc_core include directories
#  BL_LIBRARIES - The libraries needed to use blc_core
#  BL_DEFINITIONS - Compiler switches required for using blc_core

find_path(BLC_CORE_INCLUDE_DIR blc_core.h PATH_SUFFIXES blc_core )
find_library(BLC_CORE_LIBRARY blc_core)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set _FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(blc_core DEFAULT_MSG BLC_CORE_LIBRARY BLC_CORE_INCLUDE_DIR)

mark_as_advanced(BLC_CORE_INCLUDE_DIR BLC_CORE_LIBRARY )

set(BL_DEFINITIONS -pthread -Wno-multichar ${BL_DEFINITIONS})
set(BL_INCLUDE_DIRS ${BLC_CORE_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLC_CORE_LIBRARY} ${BL_LIBRARIES})	
if (UNIX AND NOT APPLE) #On MacOSX -pthread is included
set(BL_LIBRARIES  ${BL_LIBRARIES} -pthread rt)	
endif()
