/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)

 Author: Arnaud Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */


#include "blc_realtime.h"

#include <semaphore.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h> //raise
#include "blc_text.h"
#include <string.h>


int blc_pthread_errno=0;

void blc_fatal_pthread_error(const char *name_of_file, const char* name_of_function, int numero_of_line, int error_id, const char *command, const char *message, ...)
{
    char const *pthread_message;
    
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_BRIGHT_RED, stderr, "\n%s: %s \t %i:%s\n", blc_program_name, name_of_file, numero_of_line, name_of_function);
    if (command) fprintf(stderr, "Executing: %s\n", command);
    color_fprintf(BLC_BRIGHT_RED, stderr, "Pthread error:");
    switch (error_id){
        case  EPERM:pthread_message="EPERM: Operation not permited. You may try to change a statu of mutex in which you already are (i.e. lock a locked or unlock an unlocked one) or you unlock a mutex you do not hold."; break;
        case  EINVAL:  pthread_message="EINVAL: The arguments are invalid.";break;
        case  EDEADLK: pthread_message="EDEADLK: A deadlock would occur if the thread blocked waiting for mutex.";break;
        case  ENOMEM:  pthread_message="ENOMEM: Out of memory."; break;
        case  EAGAIN:  pthread_message="EAGAIN: The system lacked the necessary resources to create another thread, or the system-imposed limit on total number of threads in a process [PTHREAD_THREADS_MAX] would be exceeded."; break;
        case  ESRCH :  pthread_message="ESRCH: No thread could be found corresponding to that specified by the given thread ID, thread.";break;
        default:
            color_eprintf(BLC_BRIGHT_RED, "Error code %d unknown\n", error_id);
            pthread_message="";
    }
    
    color_eprintf(BLC_BRIGHT_RED, "%s\n", pthread_message);
    
    vfprintf(stderr, message, arguments);
    va_end(arguments);
    fprintf(stderr, "\n\n");
    fflush(stderr);
    raise(SIGABRT);
    exit(EXIT_FAILURE);
}

long blc_us_time_diff(struct timeval *previous_time)
{
   struct timeval new_time;
   long delta_useconds;

   gettimeofday(&new_time, NULL);
   delta_useconds = (new_time.tv_sec - previous_time->tv_sec) * 1000000 + new_time.tv_usec - previous_time->tv_usec;
   *previous_time = new_time;
   return delta_useconds;
}

int blc_sem_available(sem_t *sem)
{
    int ret=0;
    if( sem==NULL) EXIT_ON_ERROR("sem is NULL");
    if (sem_trywait(sem)==0) //Success to lock (it was not lock)
    {
        if (sem_trywait(sem)==0){
            SYSTEM_ERROR_CHECK(sem_post(sem), -1, NULL);
            ret=-1; //It is free at at least 2 levels
        }
        else {
            if (errno != EAGAIN) EXIT_ON_SYSTEM_ERROR("sem_trywait");
            else ret=1; //Is is free one level
        }
        
        SYSTEM_ERROR_CHECK(sem_post(sem), -1, NULL);
        return ret;
    }
    else
    {
        if (errno != EAGAIN) EXIT_ON_SYSTEM_ERROR("sem_trywait");
        return 0;
    }
}

int blc_mutex_trylock_in_time(pthread_mutex_t *mutex, uint32_t timeout){
    
    int ret;
    
    ret=pthread_mutex_trylock(mutex);
    if ( ret !=0 ){
        if (ret!=EBUSY)  EXIT_ON_SYSTEM_ERROR("wrong mutex value");
        usleep(timeout);
        ret=pthread_mutex_trylock(mutex);
        if (ret!=0){
            if (ret==EBUSY) return 0;
            else  EXIT_ON_SYSTEM_ERROR("wrong mutex value");
        }
    }
    else return 1;
    return -1;
}






