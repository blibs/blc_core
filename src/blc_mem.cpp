/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: A. Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 17/06/2014.
//

#include "blc_mem.h"

blc_mem::blc_mem():data(NULL), size(0){};

blc_mem::blc_mem(size_t size)
: data(NULL), size(size){
    data = MANY_ALLOCATIONS(size, char);
}

blc_mem::~blc_mem(){
    if ((data) && (size))
    {
        FREE(data); 
        size=0;
    }
}

void blc_mem::allocate(){
    if (data) EXIT_ON_ERROR("mem already allocated");
    else data = MANY_ALLOCATIONS(size, char);
}

void blc_mem::allocate(size_t new_size){
    
    if (new_size != size || data == NULL)  // Changement de taille ou l'allocation n'a encore jamais été faite
    {
        if (data) free(data);
        if (new_size) data = MANY_ALLOCATIONS(new_size, char);
        else data=NULL;
        size = new_size;
    }
}

void blc_mem::allocate_min(size_t new_size){
    if (new_size > size){
        ::free(data);
        data = MANY_ALLOCATIONS(new_size, char);
        size = new_size;
    }
}

void blc_mem::reallocate(size_t new_size){
    MANY_REALLOCATIONS(&chars, new_size);
    size = new_size;
}

void blc_mem::replace(char const*new_data, size_t new_size)
{
    allocate(new_size);
    memcpy(data, new_data, new_size);
}

void blc_mem::append(char const*new_data, size_t new_data_size)
{
    size_t previous_size = size;
    reallocate(size + new_data_size);
    memcpy(&chars[previous_size], new_data, new_data_size);
}

void blc_mem::append_text(char const *new_text)
{
    append(new_text, strlen(new_text));
}


void blc_mem::reset(int value){
    memset(data, value, size);
}

void blc_mem::fprint_graph_uchars(FILE *file, char const *title, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name ){
    blc_mem *this_mem=this; //This does not support &
    blc_mems_fprint_graph_uchars(&this_mem, 1, file,  &title, height, max, min, abscissa_name, ordinate_name);
}

void blc_mem::fprint_graph_floats(FILE *file, char const *title, int height, float max, float min,  char const* abscissa_name,  char const* ordinate_name ){
    blc_mem *this_mem=this; //This does not support &
    blc_mems_fprint_graph_floats(&this_mem, 1, file,  &title, height, max, min, abscissa_name, ordinate_name);
}


START_EXTERN_C
void blc_mem_allocate(blc_mem *mem, size_t size){
    mem->allocate(size);
}

void blc_mem_reallocate(blc_mem *mem, size_t new_size){
    mem->reallocate(new_size);
}

void blc_mem_replace(blc_mem *mem, const char *data, size_t size){
    mem->replace(data, size);
}

void blc_mem_append(blc_mem *mem, const char *data, size_t size){
    mem->append(data, size);
}

void blc_mems_fprint_graph_uchars(blc_mem *const*mems, int mems_nb, FILE *file, char const * const *titles, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name ){
    
    unsigned char value;
    size_t i, total_size = 0;
    int range, ordinate_name_length, ordinate_name_position;
    int j, threhold, threhold1, graph_id;
    char c;
    char vertical_arrow[] = "^|";
    
    
    ordinate_name_position = - strlen(vertical_arrow);
    
    if (ordinate_name != NULL) ordinate_name_length = strlen(ordinate_name);
    else ordinate_name_length = 0;
    
    height-=3;
    range = max - min + 1;
    
    fprintf(file, "\n");
    FOR(graph_id, mems_nb){
        for (i = fprintf(file, "100%%(%d) [%s] ", max, titles[graph_id]); i < mems[graph_id]->size * 3; ++i) fputc('-', file);
        total_size+=mems[graph_id]->size;
        fputc('|', file);
        
    }
    fprintf(file, "\n");
    
    FOR_INV(j, height){
        threhold = j * range / height;
        threhold1 = (j + 1) * range / height;
        if (ordinate_name_position < 0)
        {
            c = vertical_arrow[strlen(vertical_arrow) + ordinate_name_position];
            ordinate_name_position++;
        }
        else if (ordinate_name_position != ordinate_name_length)
        {
            c = ordinate_name[ordinate_name_position];
            ordinate_name_position++;
        }
        else c = '|';
        
        FOR(graph_id, mems_nb)
        {
            fputc(c, file);
            
            FOR(i, mems[graph_id]->size)
            {
                value = mems[graph_id]->chars[i];
                if (value - min > threhold1) fprintf(file, " []");
                else if (value - min >= threhold) {
                    fprintf(file, " %.2d", 100 * (value - min) / range);
                }
                else fprintf(file, "   ");
            }
        }
        fprintf(file, "\n");
    }
    
    FOR(graph_id, mems_nb){
        if (abscissa_name==NULL) abscissa_name="";
        for (i = fprintf(file, " 0%%(%d) %s ", min, abscissa_name); i < mems[graph_id]->size * 3; ++i)
        fputc('-', file);
        fputc('>', file);
    }
    
    fprintf(file, "\n");
}


static void fprint_graph_float_title(FILE *file, const char*title, float max, int width){
    int i;
    for (i = fprintf(file, "100%%(%.2f) [%s] ", max, title); i < width-1; ++i){
        fputc('-', file);
    }
    fputc('|', file);
}

void blc_mems_fprint_graph_floats(blc_mem *const*mems, int mems_nb, FILE *file, char const * const *titles, int height, float max, float min,  char const* abscissa_name,  char const* ordinate_name ){
    
    float value;
    size_t i, total_size = 0;
    float range;
    int ordinate_name_length, ordinate_name_position;
    int j, graph_id, percent, standard_width;
    float threhold, threhold1;
    char c;
    char vertical_arrow[] = "^|";
    
    ordinate_name_position = - strlen(vertical_arrow);
    if (ordinate_name != NULL) ordinate_name_length = strlen(ordinate_name);
    else ordinate_name_length = 0;
    
    height-=3;
    range = max - min;
    
    fprintf(file, "\n");
    
    //Standard width
    standard_width=0;
    FOR(graph_id, mems_nb) standard_width+=mems[graph_id]->size/sizeof(float) * 3;
    standard_width+=1;

    
    FOR(graph_id, mems_nb){
        fprint_graph_float_title(file, titles[graph_id], max, mems[graph_id]->size/sizeof(float) * 3);
        total_size+=mems[graph_id]->size;
    }
    fprintf(file, "\n");
    
    FOR_INV(j, height){
        threhold = j * range / height;
        threhold1 = (j+1) * range / height;
        if (ordinate_name_position < 0){
            c = vertical_arrow[strlen(vertical_arrow) + ordinate_name_position];
            ordinate_name_position++;
        }
        else if (ordinate_name_position != ordinate_name_length){
            c = ordinate_name[ordinate_name_position];
            ordinate_name_position++;
        }
        else c = '|';
        
        FOR(graph_id, mems_nb){
            fputc(c, file);
            FOR(i, mems[graph_id]->size/sizeof(float)){
                value = mems[graph_id]->floats[i];
                if (value - min > threhold1) fprintf(file, " []");
                else if (value - min >= threhold) {
                    percent=99 * (value - min) / range;
                    if (percent >= 100) percent=99;
                    fprintf(file, " %.2d", percent);
                }
                else fprintf(file, "   ");
            }
        }
        fprintf(file, "\n");
    }
    
    FOR(graph_id, mems_nb){
        if (abscissa_name==NULL) abscissa_name="";
        for (i = fprintf(file, " 0%%(%.3f) %s ", min, abscissa_name); i < mems[graph_id]->size/sizeof(float) * 3; ++i)
            fputc('-', file);
        fputc('>', file);
    }
    
    fprintf(file, "\n");
}



END_EXTERN_C
