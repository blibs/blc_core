/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 Author: A. Blanchard
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 17/06/2014.
//

#include "blc_array.h"
#include "blc_text.h"

#include <signal.h> //raise
#include <stdio.h> //fopen

START_EXTERN_C
blc_dim *vcreate_blc_dims(size_t *size, uint32_t type, int dims_nb, int length, va_list arguments){
    blc_dim *dim, *dims;
    
    dims=MANY_ALLOCATIONS(dims_nb, blc_dim);
    *size=blc_get_type_size(type);
    
    FOR_EACH(dim, dims, dims_nb){
        dim->step=*size;
        (*size)*=length;
        dim->length=length;
        length=va_arg(arguments, int);
    }
    return dims;
}

blc_dim *create_blc_dims(size_t *size, uint32_t type, int dims_nb, int length0, ...){
    blc_dim *dims;
    va_list arguments;
    va_start(arguments, length0);
    
    dims=vcreate_blc_dims(size, type, dims_nb, length0, arguments);
    
    va_end(arguments);
    return dims;
}
END_EXTERN_C

blc_array::blc_array():type('NDEF'), format('NDEF'), dims(NULL), dims_nb(0), total_length(0){}

blc_array::blc_array(uint32_t type, uint32_t format, int dims_nb, int length0, ...){
	va_list arguments;

	va_start(arguments, length0);
	vdef_array( type,  format,  dims_nb,  length0, arguments);
	allocate();
	va_end(arguments);
}

blc_array::~blc_array(){
	if (dims) FREE(dims);
}

void blc_array::vdef_array(uint32_t type, uint32_t format, int dims_nb, int length, va_list arguments){
	total_length=1;
	this->dims_nb=dims_nb;
	this->type=type;
	this->format=format;
	dims=vcreate_blc_dims(&size, type, dims_nb, length, arguments);
	total_length=size/get_type_size();
}

void blc_array::def_array(uint32_t type, uint32_t format, int dims_nb, int length, ...){
	va_list arguments;

	va_start(arguments, length);
	vdef_array(type, format, dims_nb, length, arguments);
	va_end(arguments);

}

void blc_array::def_array(uint32_t type, uint32_t format, int dims_nb, blc_dim *dims){

	this->type=type;
	this->format=format;
	this->dims_nb=dims_nb;

	size=get_type_size();
	this->dims=MANY_ALLOCATIONS(dims_nb, blc_dim);
	memcpy(this->dims, dims, sizeof(blc_dim)*dims_nb);

	size =dims[dims_nb-1].length*dims[dims_nb-1].step;
	total_length=size/get_type_size();
}

void blc_array::def_array(uint32_t type, uint32_t format, char const *dims_string){
	this->type=type;
	this->format=format;
	sscan_dims(dims_string);
}

void blc_array::init(char const *properties){
	sscan_properties(properties);
	allocate();
}

void blc_array::vinit(uint32_t type, uint32_t format, int dims_nb, int length, va_list arguments){
	vdef_array(type, format, dims_nb, length, arguments);
	allocate();
}

void blc_array::init(uint32_t type, uint32_t format, int dims_nb, int length, ...){
	va_list arguments;

	va_start(arguments, length);
	vinit(type, format, dims_nb, length, arguments);
	va_end(arguments);

}

void blc_array::add_dim(int length, int step){
	blc_dim dim;

	dim.length=length;
	dim.step=step;

	APPEND_ITEM(&dims, &dims_nb, &dim);
	size=step*length; //Check if it always true ??
			total_length=size/get_type_size();
}

void blc_array::add_dim(int length){
	if (dims_nb==0) add_dim(length, get_type_size());
	else add_dim(length, dims[dims_nb-1].length*dims[dims_nb-1].step);
}

void blc_array::set_dims(int dims_nb, int length, ...){
	blc_dim *dim;
	va_list args;

	if (dims) FREE(dims);

	this->dims_nb=dims_nb;
	dims=MANY_ALLOCATIONS(dims_nb, blc_dim);

	size=get_type_size();
	va_start(args, length);
	FOR_EACH(dim, dims,  dims_nb)
	{
		size*=length;
		dim->step=1;
		dim->length=length;
		length=va_arg(args, int);
	}
	va_end(args);
	total_length=size/get_type_size();
}

int blc_array::get_type_size(){
	return blc_get_type_size(type);
}

int blc_array::sprint_dims(char *string, int string_size){
	int i, width=0;

	if (dims_nb==0)  width=snprintf(string, string_size,  "0");
	else {
		width=snprintf(string, string_size, "%lu", dims[0].length);
		for(i=1; i<dims_nb; i++) width+=snprintf(string+width, string_size-width, "x%lu", dims[i].length);
	}
	if (width >= string_size) EXIT_ON_ERROR("The reserved size %d is too small to store the %d dims.",size, dims_nb);
	return width;
}

int blc_array::fprint_dims(FILE *file)const {
	int i, width=0;

	if (dims_nb==0)  width+=fprintf(file, "0");
	else {
		width+=fprintf(file, "%lu", dims[0].length);
		for(i=1; i<dims_nb; i++) width+=fprintf(file, "x%lu", dims[i].length);
	}
	return width;
}

void blc_array::fscan_dims(FILE *file){
	blc_dim *dim;
	int length;

	size=get_type_size();

	FSCANF(1, file, "%d", &length);
	if (dims) FREE(dims);
	dims_nb=0;

	if (length != 0){
		do{
			dim=APPEND_ALLOCATION(&dims, &dims_nb, blc_dim);
			dim->length=length;
			dim->step=size;
			size*=dim->length;
		}while(fscanf(file, "x%d", &length)==1);
	}
	total_length=size/get_type_size();
}

int blc_array::sscan_dims(char const *string){
    blc_dim *dim;
    int pos, total_pos;
    int length;
    
    size=get_type_size();
    
    SSCANF(1, string, "%d%n", &length, &pos);
    total_pos=0;
    dims_nb=0;
    if (dims) FREE(dims);
    if (length != 0){
        do{
            total_pos+=pos;
            dim=APPEND_ALLOCATION(&dims, &dims_nb, blc_dim);
            dim->length=length;
            dim->step=size;
            size*=dim->length;
        }while(sscanf(string+total_pos, "x%lu%n", &dim->length, &pos)==1);
    }
    total_length=size/get_type_size();
    return total_pos;
}


//Should use code in common with fprint_info
void blc_array::sprint_properties(char *buffer, size_t buffer_size){
	int width;
	uint32_t str_type;
	uint32_t str_format;

	if (type==0) EXIT_ON_ERROR("The type should not be NULL. Use 'NDEF' by default.");
	if (format==0) EXIT_ON_ERROR("The format should not be NULL. Use 'NDEF' by default.");

	width=snprintf(buffer, buffer_size, "%.4s %.4s ", UINT32_TO_STRING(str_type, type),  UINT32_TO_STRING(str_format, format));
	width+=sprint_dims(buffer+width,buffer_size-width);
}

void blc_array::fprint_properties(FILE *file){
	int width;
	uint32_t net_type = htonl(type);
	uint32_t net_format = htonl(format);

	if (type==0) EXIT_ON_ERROR("The type should not be NULL. Use 'NDEF' by default.");
	if (format==0) EXIT_ON_ERROR("The format should not be NULL. Use 'NDEF' by default.");


	fprintf(file, "%.4s %.4s ", (char*)&net_type, (char*)&net_format);
	width=fprint_dims(file);
}

//Should use code in common with fscan_info
void blc_array::sscan_properties(char const *string){
	int ret, pos;

	dims_nb=0;
	data=NULL;
	size=0;

	ret = sscanf(string, "%4c %4c%n", (char*)&type, (char*)&format, &pos);
	if (ret == EOF) EXIT_ON_ERROR("End of FILE");
	else if (ret!=2) EXIT_ON_ERROR("%d parameters have been read instead of 2 in '%s'.", ret,  string);

	string+=pos;

	NTOHL(type);
	NTOHL(format);

	string+=sscan_dims(string);
}

void blc_array::fscan_properties(FILE *file){
	int ret;
	dims_nb=0;
	data=NULL;
	size=0;

	ret = fscanf(file, "%4c %4c ", (char*)&type, (char*)&format);
	if (ret == EOF) EXIT_ON_ERROR("End of FILE");
	else if (ret!=2) EXIT_ON_ERROR("%d parameters have been read instead of 2", ret);

	NTOHL(type);
	NTOHL(format);

	fscan_dims(file);
}

//is it usefull ??
/*
size_t blc_array::get_minimum_size(){
    blc_dim *dim;
    size_t size = get_type_size();

    FOR_EACH(dim, dims, dims_nb) {
        if (dim->length==0) EXIT_ON_ERROR("Length must not be 0 on dim '%d'");
        size*=dim->length;
    }
    return size;
}*/

void blc_array::fprint_debug (FILE *file) const{
	uint32_t type_str, format_str;

	fprintf(file, "\nblc_array:\n type:%.4s, format:%.4s, dims_nb:%d, size:%ld \n", UINT32_TO_STRING(type_str, type), UINT32_TO_STRING(format_str, format),  dims_nb, size);
	fprint_dims(file);
	fprintf(file, "\n");
	if (data==NULL) fprintf(file, "data is null\n");
}

void blc_array::def_with_blc_file(char const *filename){
	FILE *file;
	const char *ext;

	ext=blc_get_filename_extension(filename);
	if (strcmp(ext, "blc")!=0)  EXIT_ON_ERROR("'%s' does not .blc extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening filename '%s'.", filename);

	fscan_properties(file);
	fscanf(file, "\n");
	fclose(file);
}

void blc_array::init_with_blc_file(char const *filename){
	FILE *file;
	ssize_t ret;
	const char *ext;

	ext=blc_get_filename_extension(filename);
	if (strcmp(ext, "blc")!=0)  EXIT_ON_ERROR("'%s' does not .blc extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening filename '%s'.", filename);

	fscan_properties(file);
	fscanf(file, "\n");
	allocate();
	SYSTEM_ERROR_CHECK(ret=fread(data, 1, size, file), -1, "Reading the blc_array data for '%s'", filename);
	if (ret!=size) EXIT_ON_ARRAY_ERROR(this, "Reading only '%ld' bytes for file '%s'", ret, filename);
	fclose(file);
}

void blc_array::update_with_blc_file(char const *filename){
	FILE *file;
	ssize_t ret;
	char const *ext;

	ext=blc_get_filename_extension(filename);
	if (strcmp(ext, "blc")!=0)  EXIT_ON_ERROR("'%s' does not .blc extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening filename '%s'.", filename);

	fscan_properties(file);
	fscanf(file, "\n");
	SYSTEM_ERROR_CHECK(ret=fread(data, size, 1, file), -1, "Reading the blc_array data for '%s'", filename);
	if (ret!=size) EXIT_ON_ARRAY_ERROR(this, "Reading only '%l' bytes for file '%s'", ret, filename);
	fclose(file);
}

void blc_array::save_blc_file(char const *filename){
	FILE *file;
	ssize_t ret;
	char const *ext;

	ext=blc_get_filename_extension(filename);

	//This test is mainly useful to avoid to accidently erase files.
	if (strcmp(ext, "blc")!=0)  EXIT_ON_ERROR("'%s' does not .blc extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "w"), NULL, "Opening filename '%s'.", filename);

	fprint_properties(file);
	fprintf(file, "\n");

	SYSTEM_ERROR_CHECK(ret=fwrite(data, 1, size, file), -1, "Writing the blc_array data for '%s'", filename);
	if (ret!=size) EXIT_ON_ARRAY_ERROR(this, "Writing only '%ld' bytes for file '%s'", ret, filename);
	fclose(file);
}


void blc_array::fprint_tsv(FILE *file){
	int dim, length, i, j;
	uint32_t type_str;
	blc_dim *tmp_dims;

	dim = 0;
	j = 0;

	tmp_dims = MANY_ALLOCATIONS(dims_nb, blc_dim);
	memcpy(tmp_dims, dims, dims_nb * sizeof(blc_dim));

	while (dim != dims_nb)
	{
		if (dim == 0)
		{
			length = tmp_dims[0].length;
			switch (type)
			{
			case 'INT8': if (format=='TEXT')fprintf(file, "%-*s ", length, chars);
			else FOR(i, length) fprintf(file, "%4d\nt", chars[i + j]);
			break;
			case 'UIN8':
				FOR(i, length)
				fprintf(file, "%3u\t", uchars[i + j]);
				break;
			case 'IN16':
				FOR(i, length)
				fprintf(file, "%6d\t", ints16[i + j]);
				break;
			case 'UI16':
				FOR(i, length)
				fprintf(file, "%5u\t", uints16[i + j]);
				break;
			case 'IN32':
				FOR(i, length)
				fprintf(file, "%d\t", uints32[i + j]);
				break;
			case 'UI32':
				FOR(i, length)
				fprintf(file, "%u\t", uints32[i + j]);
				break;
			case 'FL32':
				FOR(i, length)
				fprintf(file, "%f\t", floats[i + j]);
				break;
			case 'FL64':
				FOR(i, length)
				fprintf(file, "%lf\t", doubles[i + j]);
				break;
			default:
				EXIT_ON_ARRAY_ERROR(this, "You cannot display type: '%.4s'", UINT32_TO_STRING(type_str, type));
			}
			//I increment what I have just done
			j+=length;
			dim++;
		}
		else
		{
			tmp_dims[dim].length--;
			if (tmp_dims[dim].length == 0)
			{
				tmp_dims[dim].length = dims[dim].length;
				dim++;
			}
			else dim--;

			if (dim==dims_nb-2 )fprintf(file, "\n");
			else fprintf(file, "\t");
		}
	}
	fprintf(file, "\n");
	FREE(tmp_dims);
}

void blc_array::def_with_tsv_file(char const *filename){
	FILE *file;
	const char *ext;
	float value;
	int tmp_length, ret,  pos;

	ext=blc_get_filename_extension(filename);
	if (strcmp(ext, "tsv")!=0)  EXIT_ON_ERROR("'%s' does not .tsv extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening filename '%s'.", filename);

	dims_nb=0;
	do{
		tmp_length=0;
		do{
			SYSTEM_ERROR_CHECK(ret=fscanf(file, "%f", &value), -1, "Scanning file '%s'", filename);
			tmp_length++;
		}
		while(ret==1);
		this->add_dim(tmp_length);
		SYSTEM_ERROR_CHECK(ret=fscanf(file, "\n%n", &pos), -1, "Scanning file '%s'", filename);
	}while(pos==1);
	fclose(file);
}

void blc_array::update_with_tsv_file(char const *filename){
	FILE *file;
	char const *ext;
	int dim, length, j;
	blc_dim *tmp_dims;

	ext=blc_get_filename_extension(filename);
	if (strcmp(ext, "tsv")!=0)  EXIT_ON_ERROR("'%s' has not .tsv extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening filename '%s'.", filename);

	dim = 0;
	j = 0;

	tmp_dims = MANY_ALLOCATIONS(dims_nb, blc_dim);
	memcpy(tmp_dims, dims, dims_nb * sizeof(blc_dim));


	///TODO find a simpler way.
	switch (type){

	case 'FL32':
		while (dim != dims_nb)
		{
			if (dim == 0){
				length = tmp_dims[0].length;
				fscan_tsv_floats(file, &floats[j], length);
				//I increment what I have just done
				j+=length;
				dim++;
			}
			else{
				tmp_dims[dim].length--;
				if (tmp_dims[dim].length == 0){
					tmp_dims[dim].length = dims[dim].length;
					dim++;
				}
				else dim--;

				if (dim==dims_nb-2 )fscanf(file, "\n");
				else fscanf(file, "\t");
			}
		}
		break;
	case 'UIN8':
		while (dim != dims_nb)
		{
			if (dim == 0){
				length = tmp_dims[0].length;
				fscan_tsv_uchars(file, &uchars[j], length);
				//I increment what I have just done
				j+=length;
				dim++;
			}
			else{
				tmp_dims[dim].length--;
				if (tmp_dims[dim].length == 0){
					tmp_dims[dim].length = dims[dim].length;
					dim++;
				}
				else dim--;

				if (dim==dims_nb-2 )fscanf(file, "\n");
				else fscanf(file, "\t");
			}
		}
		break;
	default:
		EXIT_ON_ARRAY_ERROR(this, "This type is not working with this function. Only 'UIN8' and 'FL32' work for naw.");

	}
	FREE(tmp_dims);
	fclose(file);
}

void blc_array::save_tsv_file(char const *filename){
	FILE *file;
	char const *ext;

	ext=blc_get_filename_extension(filename);

	//This test is mainly useful to avoid to accidently erase files. At least it is a .tsv
	if (strcmp(ext, "tsv")!=0)  EXIT_ON_ERROR("'%s' has not tsv extension but '%s'", filename, ext);

	SYSTEM_ERROR_CHECK(file=fopen(filename, "w"), NULL, "Opening filename '%s' for writing.", filename);
	this->fprint_tsv(file);

	fclose(file);
}

void blc_array::fprint_surface_uchars(FILE *file, int ansi_terminal){
	blc_fprint_3Darray(file, this->uchars, this->size, 0, 1, 1, dims[0].step, dims[0].length, dims[1].step, dims[1].length, ansi_terminal);
}

/* C wrapper */
START_EXTERN_C




void blc_array_def(blc_array *array,  uint32_t type, uint32_t format, int dims_nb, int length0, ...){
	va_list arguments;

	va_start(arguments, length0);
	array->def_array(type, format, dims_nb, length0, arguments);
	va_end(arguments);
}

void blc_array_init(blc_array *array,  uint32_t type, uint32_t format, int dims_nb, int length0, ...){
	va_list arguments;

	va_start(arguments, length0);
	array->init(type, format, dims_nb, length0, arguments);
	va_end(arguments);
}

// Envoie un message d'erreur avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables. Puis exit le programme avec le parametre EXIT_FAILURE. To be used with EXIT_ON_ERROR.
void blc_array_fatal_error(blc_array const *array, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...){
	va_list arguments;
	va_start(arguments, message);
	fprintf(stderr, "\n%s: %s \t %s \t %i :\nError: ", blc_program_name, name_of_file, name_of_function, numero_of_line);
	color_vfprintf(BLC_BRIGHT_RED, stderr, message, arguments);
	va_end(arguments);
	array->fprint_debug(stderr);
	fprintf(stderr, "\n\n");
	fflush(stderr);
	raise(SIGABRT);
	exit(EXIT_FAILURE);
}

void blc_array_destroy(blc_array *array){
	array->~blc_array();
}
END_EXTERN_C
