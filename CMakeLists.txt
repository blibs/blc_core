# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
# Author: Arnaud Blanchard
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

cmake_minimum_required(VERSION 2.6)

project(blc_core)
set(CMAKE_MACOSX_RPATH 0) #avoid warning in MACOSX

add_definitions(-Wno-multichar -pthread)
include_directories(include)

set(sources
    src/blc_text.cpp
    src/blc_tools.cpp
    src/blc_mem.cpp
    src/blc_array.cpp
    src/blc_realtime.cpp)

add_library(blc_core SHARED ${sources})
add_library(static_blc_core STATIC ${sources})
if (UNIX AND NOT APPLE) #On MacOSX -pthread is included
target_link_libraries(blc_core -pthread)
endif()
#Both version of librairies will have the same name only the extension will change.
set_target_properties(static_blc_core PROPERTIES OUTPUT_NAME blc_core)
	
#Describe what will have to be installed or in the package by default the prefix (CMAKE_INSTALL_PREFIX) is '/usr/’ or '/usr/local' depending on your system
install(DIRECTORY include/ DESTINATION include/blc_core)
install(TARGETS ${PROJECT_NAME} static_blc_core DESTINATION lib)
install(FILES  ${PROJECT_SOURCE_DIR}/blc_core-config.cmake   DESTINATION share/blc_core)

#Package
set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Arnaud Blanchard")
include(CPack)

