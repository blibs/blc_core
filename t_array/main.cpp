#include "blc_core.h"
#include <math.h> //sinf

int main(int argc, char **argv){
    blc_array vector, matrix, matrix_copy, pixel_array;
    size_t i;
    int width, height;
    
    fprintf(stderr, "\nDefining a sinusoïd of 32 uchars vector\n\nProperties:\n");
    //We define the properties of an array of type unsigned char (UIN8) with undef format (i.e. the user use it as he wants), of one dimension (vector) of 32 values.
    vector.def_array('UIN8', 'NDEF', 1, 32);
    //The property .dims (list of all dimensions) has been allocated.
    
    //Be careful the .data memory is not allocated ! This is usefull if you want to associate the data to your own memory.
    //We display the properties
    vector.fprint_properties(stderr);
    fprintf(stderr, "\n");
    
    //We allocate the memory
    vector.allocate();
    
    // We set a sinusoïd in the vector (converting [-1f, 1f] -> [0, 255];
    for(i=0; i!=vector.size; i++) vector.uchars[i]=128+127*sinf(i/2.f);
    
    fprintf(stderr, "Graph of the vector\n");

    //We graph the vector, with title "vector test", the height of 16 chars, the limit max 256 and limit min 0.
    //Text on abscissa "position" and text on ordonate "intensity"
    vector.fprint_graph_uchars(stderr, "Vector test", 16, 256, 0, "position", "intensity");

    fprintf(stderr, "\nDefining 3x5 float matrix\n\nDims:\n");

    //We define a matrix of 3x5
    matrix.def_array('FL32', 'NDEF', 2, 3, 5);
    
    //We allocate the content
    matrix.allocate();
    
    //We display  the dim sizes
    matrix.fprint_dims(stderr);
    fprintf(stderr, "\n");
    
    //We acquire the sizes of the dims
    width= matrix.dims[0].length;
    height= matrix.dims[1].length;
    fprintf(stderr, "width:%d, height:%d\n", width, height);
    
    //We set the data to zero everywhere
    memset(matrix.data, '0', matrix.size);
    
    //We set the first column of the matrix to 6.3
    for(i=0; i!=height; i++) matrix.floats[i*width]=6.3;
    
    //Write 33.3 in the center
    matrix.floats[1+width*2]=33.3;
    matrix.fprint_tsv(stderr);
    
    //We save the value as a blc file that we will be able to reread
    matrix.save_blc_file("blibs/blc_core/t_array/array.blc");
    matrix.save_tsv_file("blibs/blc_core/t_array/array.tsv"); //Bigger but readable in text mode or within spreadsheet, excel, matlab
    
    //We reload the matrix which should be identical
    matrix_copy.init_with_blc_file("blibs/blc_core/t_array/array.blc");
    
    //we compare the content of the two matrixes
    if (memcmp(matrix.data, matrix_copy.data, matrix.size)!=0) EXIT_ON_ARRAY_ERROR(&matrix,"The content of the saved and reloaded matrix are not the same");
    
    fprintf(stderr, "\nWe load an array of intensities and display the values\n\n");
    
    //Load the array
    pixel_array.init_with_blc_file("blibs/blc_core/lena32.blc");
    
    //Display the array as a array of decimals)
    pixel_array.fprint_surface_uchars(stderr);
    
    fprintf(stderr, "\nWe display the array of intensities as compact colored values\n\n");
    
    //Display the array as a compact surface of color (ansi_terminal). Ideal for images
    pixel_array.fprint_surface_uchars(stderr, 1);
    
    fprintf(stderr, "\n");
    //Print the scale
    blc_fprint_color_scale(stderr);
    
    return 0;
}
